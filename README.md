## Basic Online Store in Symfony 4.2 
> (style: Bootstrap 4, admin panel: EasyAdminBundle, login system: FOSUserBundle, registration conformation via email)

### Requirements
* composer (latest)
* php >= 7.0
* mysql-server >= 5.7

### Installation & Preparing
Load dependencies:
```
composer install
```
Fill these parameters in the `.env` file:
```
DATABASE_URL=mysql://username:password@127.0.0.1:3306/dbname
MAILER_URL=gmail://username:password@localhost
SENDER_EMAIL=...
SENDER_NAME=...
STRIPE_API_KEY=...
```
Create DB & fake entities:
```
bin/console doctrine:database:create
bin/console doctrine:migration:migrate
bin/console hautelook:fixtures:load
bin/console server:run
```

### Users credentials
| username | password | role  |
|----------|----------|-------|
| admin    | 123qwe   | admin |