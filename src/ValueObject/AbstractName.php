<?php
declare(strict_types = 1);

namespace App\ValueObject;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class AbstractName
{
    const MIN_LENGTH = 3;
    const MAX_LENGTH = 255;
}