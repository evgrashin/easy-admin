<?php

namespace App\ValueObject;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class CurrencyValue
{
    const LIST = ['RUB'];
    const DEFAULT = 'RUB';
}