<?php
declare(strict_types = 1);

namespace App\ValueObject;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class UserName extends AbstractName
{
    const MAX_LENGTH = 100;
}