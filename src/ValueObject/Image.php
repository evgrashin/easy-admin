<?php

namespace App\ValueObject;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class Image
{
    const DEFAULT = "default.jpg";
    const MAX_SIZE = '1M';
    const MAX_HEIGHT = '1000';
    const MAX_WIDTH = '1000';
    const INVALID_TYPE_MESSAGE = 'This file is not a valid image (jpeg, png)';
}