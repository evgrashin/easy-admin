<?php
declare(strict_types = 1);

namespace App\ValueObject;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class Quantity
{
    const MIN = 1;
    const MAX = 999;
}