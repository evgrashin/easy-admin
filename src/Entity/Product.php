<?php
declare(strict_types = 1);

namespace App\Entity;

use App\ValueObject\CurrencyValue;
use App\ValueObject\Image;
use Doctrine\ORM\Mapping as ORM;
use Money\Currency;
use Money\Money;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use App\Validator\Constraints;
use App\Traits\ValueListTrait;
use App\ValueObject\ProductName;
use App\Validator\Constraints\Sku;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ORM\Table(name="products")
 * @UniqueEntity(fields="title", message="This title is already used")
 * @UniqueEntity(fields="sku", message="This SKU is already used")
 * @Vich\Uploadable
 */
class Product
{
    use ValueListTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=Sku::MAX_LENGTH, unique=true)
     * @Assert\NotBlank()
     * @Constraints\Sku()
     *
     * @var string
     */
    private $sku;

    /**
     * @ORM\Column(type="string", length=ProductName::MAX_LENGTH, unique=true)
     * @Assert\Length(min=ProductName::MIN_LENGTH, max=ProductName::MAX_LENGTH)
     * @Assert\NotBlank()
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="text", length=500)
     * @Assert\NotBlank()
     *
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(type="money")
     * @ORM\Embedded(class="\Money\Money")
     * @Assert\NotBlank()
     *
     * @var Money
     */
    private $price;

    /**
     * @ORM\Column(type="boolean")
     *
     * @var bool
     */
    private $published;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="image")
     * @Assert\Image(
     *     maxHeight=Image::MAX_HEIGHT,
     *     maxWidth=Image::MAX_WIDTH,
     *     maxSize=Image::MAX_SIZE,
     *     mimeTypes={"image/png", "image/jpeg"},
     *     mimeTypesMessage=Image::INVALID_TYPE_MESSAGE
     * )
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="products")
     * @ORM\JoinColumn(nullable=true)
     *
     * @var Category
     */
    private $category;

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $currency = new Currency(CurrencyValue::DEFAULT);
        $this->price = new Money(0, $currency);
        $this->published = false;
        $this->category = null;
    }

    /**
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getSku(): ?string
    {
        return $this->sku;
    }

    /**
     * @param string|null $sku
     */
    public function setSku(?string $sku): void
    {
        $this->sku = $sku;
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return null|Money
     */
    public function getPrice(): ?Money
    {
        return $this->price;
    }

    /**
     * @param Money|null $price
     * @return Product
     */
    public function setPrice(?Money $price): Product
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return null|Category
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory(Category $category): void
    {
        $this->category = $category;
    }

    /**
     * @return boolean
     */
    public function isPublished(): bool
    {
        return $this->published;
    }

    /**
     * @param boolean $published
     */
    public function setPublished(bool $published): void
    {
        $this->published = $published;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image ?? Image::DEFAULT;
    }

    /**
     * @param string|null $image
     */
    public function setImage(?string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return File|null
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @param File $imageFile
     */
    public function setImageFile(File $imageFile): void
    {
        $this->imageFile = $imageFile;
    }

    /**
     * @Assert\Callback
     * @param ExecutionContextInterface $context
     */
    public function validate(ExecutionContextInterface $context): void
    {
        if ($this->price->getAmount() <= 0) {
            $context->buildViolation('Price should be more than zero')
                ->atPath('price')
                ->addViolation();
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->title;
    }
}