<?php
declare(strict_types = 1);

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use App\ValueObject\CategoryName;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 * @ORM\Table(name="categories")
 * @UniqueEntity("name")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(unique=true, length=CategoryName::MAX_LENGTH)
     * @Assert\NotBlank()
     * @Assert\Length(min=CategoryName::MIN_LENGTH, max=CategoryName::MAX_LENGTH)
     * @var string
     */
    private $name;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Product",
     *     mappedBy="category",
     *     cascade={"remove"}
     * )
     *
     * @var Product[]
     */
    private $products;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $published = true;

    /**
     * Category constructor.
     */
    public function __construct()
    {
        $this->products = null;
        $this->published = true;
    }

    /**
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): ?Collection
    {
        return $this->products;
    }

    /**
     * @param Collection|Product[] $products
     */
    public function setProducts(?Collection $products): void
    {
        $this->products = $products;
    }
    /**
     * @return boolean
     */
    public function isPublished(): bool
    {
        return $this->published;
    }

    /**
     * @param boolean $published
     */
    public function setPublished(bool $published): void
    {
        $this->published = $published;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->name;
    }
}