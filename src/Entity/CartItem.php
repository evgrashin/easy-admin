<?php
declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Money\Money;
use Symfony\Component\Validator\Constraints as Assert;
use App\ValueObject\Quantity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CartItemRepository")
 * @ORM\Table(
 *     name="cart_items",
 *     uniqueConstraints={
 *        @UniqueConstraint(
 *            name="cart_item_unique",
 *            columns={"product_id", "shopping_cart_id"})
 *    })
 */
class CartItem
{
    /**
    * @ORM\Id()
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    * @var integer
    */
    private $id;

    /**
     * @ORM\JoinColumn(nullable=false)
     * @ORM\ManyToOne(
     *     targetEntity="Product"
     * )
     * @var Product
     */
    private $product;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(min=Quantity::MIN, max=Quantity::MAX)
     * @var integer
     */
    private $quantity = 1;

    /**
     * @ORM\JoinColumn(nullable=false)
     * @ORM\ManyToOne(
     *     targetEntity="ShoppingCart",
     *     inversedBy="cartItems"
     * )
     * @var ShoppingCart
     */
    private $shoppingCart;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @var \DateTimeImmutable
     */
    private $createAt;

    /**
     * @param Product      $product
     * @param ShoppingCart $shoppingCart
     * @param integer      $quantity
     * @throws \Exception Exception in case of an error.
     */
    public function __construct(Product $product, ShoppingCart $shoppingCart, int $quantity = 1)
    {
        $this->product      = $product;
        $this->shoppingCart = $shoppingCart;
        if ($quantity > 0) {
            $this->quantity = $quantity;
        }
        $this->createAt     = new \DateTimeImmutable();
    }

    /**
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return integer
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param integer $quantity
     */
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return ShoppingCart
     */
    public function getShoppingCart(): ShoppingCart
    {
        return $this->shoppingCart;
    }

    /**
     * @param ShoppingCart $shoppingCart
     */

    /**
     * @return \DateTimeImmutable
     */
    public function getCreateAt(): \DateTimeImmutable
    {
        return $this->createAt;
    }

    /**
     * @param integer $quantity
     * @return void
     */
    public function increaseQuantity(int $quantity = 1): void
    {
        if ($quantity < 1) {
            return ;
        }
        $this->quantity = $this->quantity + $quantity;
    }

    /**
     * @param integer $quantity
     * @return void
     */
    public function reduceQuantity(int $quantity = 1): void
    {
        if ($this->quantity < $quantity) {
            return ;
        }
        $this->quantity = $this->quantity - $quantity;
    }

    /**
     * @return Money
     */
    public function getCost(): Money
    {
        $quantity = $this->quantity;
        $price = $this->getProduct()->getPrice();
        $cost = $price;

        for ($i = 1; $i < $quantity; $i++) {
            $cost = $cost->add($price);
        }

        return $cost;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf('%s (X %d)', $this->getProduct()->getTitle(), $this->getQuantity());
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->shoppingCart->getUser();
    }
}