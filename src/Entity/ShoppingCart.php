<?php
declare(strict_types = 1);

namespace App\Entity;

use App\Repository\CartItemRepositoryInterface;
use App\ValueObject\CurrencyValue;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Money\Currency;
use Money\Money;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Tbbc\MoneyBundle\Pair\PairManagerInterface;
use Stripe\Charge;
use Stripe\Stripe;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShoppingCartRepository")
 * @ORM\Table(name="shopping_carts")
 * @UniqueEntity(fields="changeId", message="This title is already used")
 */
class ShoppingCart
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\JoinColumn()
     * @ORM\ManyToOne(
     *     targetEntity="User",
     *     inversedBy="shoppingCart"
     * )
     * @var User
     */
    private $user;

    /**
     * @ORM\OneToMany(
     *     targetEntity="CartItem",
     *     mappedBy="shoppingCart",
     *     cascade={"remove"}
     * )
     * @var CartItem[]
     */
    private $cartItems;

    /**
     * @ORM\Column(type="string", nullable=true, unique=true)
     * @var string
     */
    private $chargeId;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $confirmed = false;

    /**
     * @ORM\Column(type="datetime_immutable")
     * @var \DateTimeImmutable
     */
    private $createAt;

    /**
     * @param User $user
     * @throws \Exception Exception in case of an error.
     */
    public function __construct(User $user)
    {
        $this->user      = $user;
        $this->cartItems = new ArrayCollection();
        $this->createAt  = new \DateTimeImmutable();
    }

    /**
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @return CartItem[]|Collection
     */
    public function getCartItems()
    {
        return $this->cartItems;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreateAt(): \DateTimeImmutable
    {
        return $this->createAt;
    }

    /**
     * @param Product $product
     * @return boolean
     */
    public function hasProduct(Product $product): bool
    {
        $items = $this->getCartItems();
        foreach ($items as $item) {
            if ($product === $item->getProduct()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param PairManagerInterface $pairManager
     * @return Money
     */
    public function getTotalPrice(PairManagerInterface $pairManager): Money
    {
        $currency = new Currency(CurrencyValue::DEFAULT);
        $totalPrice = new Money(0, $currency);

        foreach ($this->cartItems as $item) {
            $itemPrice = $item->getCost();
            $itemPriceConverted = $pairManager->convert($itemPrice, CurrencyValue::DEFAULT);
            $totalPrice = $totalPrice->add($itemPriceConverted);
        }

        return $totalPrice;
    }

    /**
     * @return integer
     */
    public function getTotalQuantity(): int
    {
        $totalQuantity = 0;

        foreach ($this->cartItems as $item) {
            $totalQuantity += $item->getQuantity();
        }

        return $totalQuantity;
    }

    /**
     * @return string|null
     */
    public function getChargeId(): ?string
    {
        return $this->chargeId;
    }

    /**
     * @param string|null $chargeId
     */
    public function setChargeId(?string $chargeId): void
    {
        $this->chargeId = $chargeId;
    }

    /**
     * @return boolean
     */
    public function getConfirmed(): bool
    {
        return $this->confirmed;
    }

    /**
     * @param boolean $confirmed
     */
    public function setConfirmed(bool $confirmed): void
    {
        if (null === $this->chargeId) {
            return;
        }
        $this->confirmed = $confirmed;
    }

    /**
     * @return boolean
     */
    public function hasChargeId(): bool
    {
        if (null !== $this->chargeId) {
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf('%d', $this->getId());
    }

    /**
     * @param CartItemRepositoryInterface $cartItems
     * @param Product                     $product
     * @param integer                     $quantity
     * @return self
     * @throws \Exception Emmet exception in error case.
     */
    public function addItem(CartItemRepositoryInterface $cartItems, Product $product, int $quantity = 1): self
    {
        /** @var CartItem $cartItem */
        $cartItem = $cartItems->findCartItemByProduct($product, $this);
        if (null !== $cartItem) {
            $cartItem->increaseQuantity($quantity);
        } else {
            $cartItem = new CartItem($product, $this, $quantity);
        }

        $cartItems->save($cartItem);

        return $this;
    }

    /**
     * @param CartItemRepositoryInterface $cartItems
     * @param CartItem                    $cartItem
     * @return void
     */
    public function removeItem(CartItemRepositoryInterface $cartItems, CartItem $cartItem): void
    {
        $cartItems->remove($cartItem);
    }

    /**
     * @param CartItemRepositoryInterface $cartItems
     * @param CartItem                    $cartItem
     * @return void
     */
    public function incrementItemQuantity(CartItemRepositoryInterface $cartItems, CartItem $cartItem): void
    {
        $cartItem->increaseQuantity();
        $cartItems->save($cartItem);
    }

    /**
     * @param CartItemRepositoryInterface $cartItems
     * @param CartItem                    $cartItem
     * @return void
     */
    public function decrementItemQuantity(CartItemRepositoryInterface $cartItems, CartItem $cartItem): void
    {
        $cartItem->reduceQuantity();
        $cartItems->save($cartItem);
    }

    /**
     * @return Money|null
     */
    public function getChargePrice(): ?Money
    {
        Stripe::setApiKey(getenv('STRIPE_API_KEY'));
        if (null === $this->chargeId) {
            return null;
        } else {
            /** @var Charge $charge */
            $charge = Charge::retrieve($this->chargeId);
            $currency = new Currency(strtoupper($charge->currency));
            $price = new Money($charge->amount, $currency);

            return $price;
        }
    }
}