<?php
declare(strict_types = 1);

namespace App\Traits;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
trait ValueListTrait
{
    /**
     * Returns entity translation prefix.
     *
     * @return string
     * @throws \ReflectionException If the class does not exist.
     */
    protected static function getLowerCaseClassName()
    {
        $refClass = new \ReflectionClass(get_called_class());
        $className = $refClass->getShortName();

        return strtolower($className);
    }

    /**
     * Returns values list, with or without labels, for a given property.
     *
     * @param string  $property
     * @param boolean $withLabelsAsIndexes
     * @param array   $filterValues
     *
     * @return array
     * @throws \ReflectionException If the class does not exist.
     */
    public static function getValuesList(string $property, bool $withLabelsAsIndexes = false, array $filterValues = null)
    {
        $propertyValues = array();
        $refClass = new \ReflectionClass(get_called_class());
        $classConstants = $refClass->getConstants();

        $constantPrefix = strtoupper($property).'_';
        foreach ($classConstants as $key => $val) {
            if (substr($key, 0, strlen($constantPrefix)) === $constantPrefix) {
                $propertyValues[$val] = static::getLowerCaseClassName().'.'.strtolower($property).'.'.$val;
            }
        }

        if (isset($filterValues)) {
            $propertyValues = array_filter($propertyValues, function ($key) use ($filterValues) {
                return in_array($key, $filterValues);
            }, ARRAY_FILTER_USE_KEY);
        }

        if ($withLabelsAsIndexes) {
            return array_flip($propertyValues);
        }

        return array_keys($propertyValues);
    }

    /**
     * Checks that a value is valid for a given property.
     *
     * @param string $property
     * @param mixed  $value
     *
     * @throws \ReflectionException If the class does not exist.
     */
    public static function checkAllowedValue(string $property, $value)
    {
        if (!in_array($value, static::getValuesList($property))) {
            throw new \InvalidArgumentException(
                sprintf('"%s" is not a valid value for "%s" property !', (string) $value, $property)
            );
        }
    }

    /**
     * Get value label.
     *
     * @param string $property
     *
     * @return string
     * @throws \ReflectionException If the class does not exist.
     */
    public function getValueLabel(string $property)
    {
        $valueList = array_flip($this->getValuesList($property, true));

        return isset($valueList[$this->{$property}]) ? $valueList[$this->{$property}] : $this->{$property};
    }
}
