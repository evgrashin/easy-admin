<?php
declare(strict_types = 1);

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Product;
use App\Query\ProductSearchQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class ProductRepository extends AbstractEntityRepository implements ProductRepositoryInterface
{
    const ALIAS_PRODUCT    = 'product';
    const ALIAS_CATEGORY   = 'category';
    const COLUMN_ID        = 'id';
    const COLUMN_SKU       = 'sku';
    const COLUMN_PUBLISHED = 'published';
    const COLUMN_CATEGORY  = 'category';
    const SORT_DIRECTIONS  = ['asc', 'desc'];
    const TRUE             = 'true';

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, Product::class);
    }

    /**
     * @param ProductSearchQuery $query
     * @return Query
     */
    public function getFindAllPublishedQuery(ProductSearchQuery $query): Query
    {
        $queryBuilder = $this->initQueryBuilderWithSearchQuery($query);
        $queryBuilder = $this->addWherePublishedTrue($queryBuilder);

        return $queryBuilder->getQuery();
    }

    /**
     * @param ProductSearchQuery $query
     * @param Category           $category
     * @return Query
     */
    public function getFindAllPublishedByCategoryQuery(ProductSearchQuery $query, Category $category): Query
    {
        $queryBuilder = $this->initQueryBuilderWithSearchQuery($query);
        $queryBuilder = $this->addWherePublishedTrue($queryBuilder);
        $queryBuilder = $this->addWhereCategory($queryBuilder, $category);

        return $queryBuilder->getQuery();
    }

    /**
     * @param ProductSearchQuery $query
     * @return QueryBuilder
     */
    public function initQueryBuilderWithSearchQuery(ProductSearchQuery $query): QueryBuilder
    {
        $sortBy  = $query->getSortBy();
        $sortDir = $query->getSortDir();

        $availableSortBy = $this->getSortableFieldNames();
        if (!in_array($sortBy, $availableSortBy)) {
            $sortBy = self::COLUMN_ID;
        }

        if (!in_array($sortDir, self::SORT_DIRECTIONS)) {
            $sortDir = self::SORT_DIRECTION_DESC;
        }

        return $this->createQueryBuilder(self::ALIAS_PRODUCT)
            ->orderBy(self::ALIAS_PRODUCT.'.'.$sortBy, $sortDir);
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @return QueryBuilder
     */
    public function addWherePublishedTrue(QueryBuilder $queryBuilder): QueryBuilder
    {
        return $queryBuilder
            ->andWhere(self::ALIAS_PRODUCT.'.'.self::COLUMN_PUBLISHED.'='.self::TRUE)
            ->join(self::ALIAS_PRODUCT.'.'.self::COLUMN_CATEGORY, self::COLUMN_CATEGORY)
            ->andWhere(self::COLUMN_CATEGORY.'.'.self::COLUMN_PUBLISHED.'='.self::TRUE);
    }

     /**
     * @param QueryBuilder $queryBuilder
     * @param Category     $category
     * @return QueryBuilder
     */
    public function addWhereCategory(QueryBuilder $queryBuilder, Category $category): QueryBuilder
    {
        return $queryBuilder
            ->andWhere(self::ALIAS_PRODUCT.'.'.self::COLUMN_CATEGORY.'=:'.self::COLUMN_CATEGORY)
            ->setParameter(self::COLUMN_CATEGORY, $category);
    }

    /**
     * {@inheritdoc}
     */
    public function getSortableFieldNames(): array
    {
        return $this->_class->getFieldNames();
    }

    /**
     * {@inheritdoc}
     * @throws NonUniqueResultException If result is not unique.
     */
    public function findOneBySku(string $sku): ?Product
    {
        $query = $this->createQueryBuilder(self::ALIAS_PRODUCT)
            ->where(self::ALIAS_PRODUCT.'.'.self::COLUMN_SKU .'=:'.self::COLUMN_SKU)
            ->setParameter(self::COLUMN_SKU, $sku)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * @param Product $product
     * @throws \Doctrine\ORM\ORMException Emmet exception in error case.
     * @throws \Doctrine\ORM\OptimisticLockException Emmet exception in error case.
     */
    public function save(Product $product): void
    {
        $this->_em->persist($product);
        $this->_em->flush();
    }
}