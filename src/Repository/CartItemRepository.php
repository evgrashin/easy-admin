<?php
declare(strict_types = 1);

namespace App\Repository;

use App\Entity\CartItem;
use App\Entity\Product;
use App\Entity\ShoppingCart;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class CartItemRepository extends AbstractEntityRepository implements CartItemRepositoryInterface
{
    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, CartItem::class);
    }

    /**
     * {@inheritdoc}
     * @throws \Doctrine\ORM\ORMException Emmet exception in error case.
     */
    public function save(CartItem $cartItem): void
    {
        $this->_em->persist($cartItem);
        $this->_em->flush();
    }

    /**
     * {@inheritdoc}
     * @throws \Doctrine\ORM\ORMException Emmet exception in error case.
     */
    public function remove(CartItem $cartItem): void
    {
        $this->_em->remove($cartItem);
        $this->_em->flush();
    }

    /**
     * {@inheritdoc}
     * @throws NonUniqueResultException If result is not unique.
     */
    public function findCartItemByProduct(Product $product, ShoppingCart $shoppingCart): ?CartItem
    {
        $query = $this->createQueryBuilder('item')
            ->where('item.product = :product')
            ->setParameter('product', $product)
            ->andWhere('item.shoppingCart = :shoppingCart')
            ->setParameter('shoppingCart', $shoppingCart)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * {@inheritdoc}
     * @throws NonUniqueResultException If result is not unique.
     */
    public function itemInCart(Product $product, ShoppingCart $shoppingCart): bool
    {
        if (null === $this->findCartItemByProduct($product, $shoppingCart)) {
            return false;
        }

        return true;
    }
}