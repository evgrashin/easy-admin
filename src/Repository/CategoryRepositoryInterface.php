<?php
declare(strict_types = 1);

namespace App\Repository;

use App\Entity\Category;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
interface CategoryRepositoryInterface
{
    /**
     * @return Category[]
     */
    public function findAllNotEmpty(): array;
}