<?php
declare(strict_types = 1);

namespace App\Repository;

use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class CategoryRepository extends AbstractEntityRepository implements CategoryRepositoryInterface
{
    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, Category::class);
    }

    /**
     * @return Category[]
     */
    public function findAllNotEmpty(): array
    {
        $query = $this->createQueryBuilder('category')
            ->select(['category.id', 'category.name'])
            ->addSelect('count(products.id) AS products_count')
            ->andWhere('category.published = true')
            ->join('category.products', 'products')
            ->andWhere('products.published = true')
            ->groupBy('category.id')
            ->getQuery();

        return $query->getResult();
    }
}