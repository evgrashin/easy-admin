<?php
declare(strict_types = 1);

namespace App\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class AbstractEntityRepository extends EntityRepository
{
    /**
     * @param EntityManagerInterface $em
     * @param string                 $className
     */
    public function __construct(EntityManagerInterface $em, string $className)
    {
        $classMetadata = $em->getClassMetadata($className);
        parent::__construct($em, $classMetadata);
    }
}
