<?php
declare(strict_types = 1);

namespace App\Repository;

use App\Entity\ShoppingCart;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class ShoppingCartRepository extends AbstractEntityRepository implements ShoppingCartRepositoryInterface
{
    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, ShoppingCart::class);
    }

    /**
     * {@inheritdoc}
     * @throws \Exception Emmet exception in error case.
     */
    public function findOneByUserOrCreate(User $user): ShoppingCart
    {
        $query = $this->createQueryBuilder('cart')
            ->andWhere('cart.chargeId IS NULL')
            ->andWhere('cart.user = :user')
            ->setParameter('user', $user)
            ->orderBy('cart.createAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery();

        $result = $query->getOneOrNullResult();
        if (null === $result) {
            return $this->createForUser($user);
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception Emmet exception in error case.
     */
    public function createForUser(User $user): ShoppingCart
    {
        $shoppingCart = new ShoppingCart($user);
        $this->_em->persist($shoppingCart);
        $this->_em->flush();

        return $shoppingCart;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception Emmet exception in error case.
     */
    public function save(ShoppingCart $shoppingCart): ShoppingCart
    {
        $this->_em->persist($shoppingCart);
        $this->_em->flush();

        return $shoppingCart;
    }

    /**
     * {@inheritdoc}
     */
    public function findAllByUser(User $user): array
    {
        $query = $this->createQueryBuilder('cart')
            ->andWhere('cart.chargeId IS NOT NULL')
            ->andWhere('cart.user = :user')
            ->setParameter('user', $user)
            ->orderBy('cart.createAt', 'DESC')
            ->getQuery();

        return $query->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function getFieldNames(): array
    {
        return $this->_class->getFieldNames();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllCharged(): array
    {
        $query = $this->createQueryBuilder('cart')
            ->andWhere('cart.chargeId IS NOT NULL')
            ->join('cart.user', 'user')
            ->join('cart.cartItems', 'cartItems')
            ->orderBy('cart.confirmed')
            ->getQuery();

        return $query->getResult();
    }

    /**
     * @param string $id
     * @return ShoppingCart|null
     * @throws NonUniqueResultException If result is not unique.
     */
    public function findById(string $id): ?ShoppingCart
    {
        $query = $this->createQueryBuilder('cart')
            ->where('cart.id = :id')
            ->setParameter('id', $id)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * @param string $chargeId
     * @return ShoppingCart|null
     * @throws NonUniqueResultException If result is not unique.
     */
    public function findByChargeId(string $chargeId): ?ShoppingCart
    {
        $query = $this->createQueryBuilder('cart')
            ->where('cart.chargeId = :chargeId')
            ->setParameter('chargeId', $chargeId)
            ->getQuery();

        return $query->getOneOrNullResult();
    }
}