<?php
declare(strict_types = 1);

namespace App\Repository;

use App\Entity\CartItem;
use App\Entity\Product;
use App\Entity\ShoppingCart;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
interface CartItemRepositoryInterface
{
    /**
     * @param CartItem $cartItem
     * @return void
     */
    public function save(CartItem $cartItem): void;

    /**
     * @param CartItem $cartItem
     * @return void
     */
    public function remove(CartItem $cartItem): void;

    /**
     * @param Product      $product
     * @param ShoppingCart $shoppingCart
     * @return CartItem|null
     */
    public function findCartItemByProduct(Product $product, ShoppingCart $shoppingCart): ?CartItem;

    /**
     * @param Product      $product
     * @param ShoppingCart $shoppingCart
     * @return boolean
     */
    public function itemInCart(Product $product, ShoppingCart $shoppingCart): bool;
}