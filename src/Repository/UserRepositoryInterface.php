<?php
declare(strict_types = 1);

namespace App\Repository;

use App\Entity\User;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
interface UserRepositoryInterface
{
    /**
     * @param string $email
     * @return User|null
     */
    public function findOneByEmail(string $email): ?User;

    /**
     * @param string $username
     * @return User|null
     */
    public function findOneByUsername(string $username): ?User;
}