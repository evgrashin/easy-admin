<?php
declare(strict_types = 1);

namespace App\Repository;

use App\Entity\ShoppingCart;
use App\Entity\User;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
interface ShoppingCartRepositoryInterface
{
    /**
     * @param User $user
     * @return ShoppingCart
     */
    public function findOneByUserOrCreate(User $user): ShoppingCart;

    /**
     * @param User $user
     * @return ShoppingCart
     */
    public function createForUser(User $user): ShoppingCart;

    /**
     * @param ShoppingCart $shoppingCart
     * @return mixed
     */
    public function save(ShoppingCart $shoppingCart): ShoppingCart;

    /**
     * @param User $user
     * @return mixed
     */
    public function findAllByUser(User $user): array;

    /**
     * @return array
     */
    public function getFieldNames(): array;

    /**
     * @return array
     */
    public function findAllCharged(): array;

    /**
     * @param string $id
     * @return ShoppingCart|null
     */
    public function findById(string $id): ?ShoppingCart;

    /**
     * @param string $chargeId
     * @return ShoppingCart|null
     */
    public function findByChargeId(string $chargeId): ?ShoppingCart;
}