<?php
declare(strict_types = 1);

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Product;
use App\Query\ProductSearchQuery;
use Doctrine\ORM\Query;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
interface ProductRepositoryInterface
{
    const SORT_DIRECTION_DESC = 'desc';
    const MAX_RESULTS         = 8;

    /**
     * @param ProductSearchQuery $query
     * @return Query
     */
    public function getFindAllPublishedQuery(ProductSearchQuery $query): Query;

    /**
     * @param ProductSearchQuery $query
     * @param Category           $category
     * @return Query
     */
    public function getFindAllPublishedByCategoryQuery(ProductSearchQuery $query, Category $category): Query;

    /**
     * @return array|string[]
     */
    public function getSortableFieldNames(): array;

    /**
     * @param string $sku
     * @return Product|null
     */
    public function findOneBySku(string $sku): ?Product;

    /**
     * @param Product $product
     * @return void
     */
    public function save(Product $product): void;
}