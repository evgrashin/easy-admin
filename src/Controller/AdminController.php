<?php
declare(strict_types = 1);

namespace App\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class AdminController extends EasyAdminController
{
    /**
     * @var UserManagerInterface
     */
    protected $manager;
    /**
     * @var MailerInterface
     */
    private $mailer;
    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;

    /**
     * AdminController constructor.
     * @param UserManagerInterface    $manager
     * @param TokenGeneratorInterface $tokenGenerator
     * @param MailerInterface         $mailer
     */
    public function __construct(
        UserManagerInterface $manager,
        TokenGeneratorInterface $tokenGenerator,
        MailerInterface $mailer
    ) {
        $this->manager = $manager;
        $this->tokenGenerator = $tokenGenerator;
        $this->mailer = $mailer;
    }

    /**
     * @return UserInterface
     */
    public function createNewUserEntity(): UserInterface
    {
        return $this->manager->createUser();
    }

    /**
     * @param UserInterface $user
     */
    public function persistUserEntity(UserInterface $user): void
    {
        $user->setEnabled(false);
        if (null === $user->getConfirmationToken()) {
            $user->setConfirmationToken($this->tokenGenerator->generateToken());
        }
        $this->manager->updateUser($user);
        $this->mailer->sendConfirmationEmailMessage($user);
        parent::persistEntity($user);
    }

    /**
     * @param UserInterface $user
     */
    public function updateUserEntity(UserInterface $user): void
    {
        $this->manager->updateUser($user);
        parent::updateEntity($user);
    }
}