<?php declare(strict_types = 1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class MainController extends AbstractController
{
    /**
     * @Route(path="/", name="homepage")
     * @return Response
     */
    public function mainAction(): Response
    {
        return new Response($this->renderView('index.html.twig'));
    }
}