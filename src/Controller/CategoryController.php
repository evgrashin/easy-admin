<?php
declare(strict_types = 1);

namespace App\Controller;

use App\Entity\Category;
use App\Query\ProductSearchQuery;
use App\Repository\CategoryRepositoryInterface;
use App\Repository\ProductRepository;
use App\Repository\ProductRepositoryInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route(path="/category")
 */
class CategoryController extends AbstractController
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $categories;
    /**
     * @var ProductRepositoryInterface
     */
    private $products;
    /**
     * @var PaginatorInterface
     */
    private $paginator;

    /**
     * @param ProductRepositoryInterface  $products
     * @param CategoryRepositoryInterface $categories
     * @param PaginatorInterface          $paginator
     */
    public function __construct(
        ProductRepositoryInterface $products,
        CategoryRepositoryInterface $categories,
        PaginatorInterface $paginator
    ) {
        $this->categories = $categories;
        $this->products = $products;
        $this->paginator = $paginator;
    }

    /**
     * @Route(path="/{id}", name="category_product_list")
     * @Method({"GET", "POST"})
     * @param Request  $request
     * @param Category $category
     * @return Response
     */
    public function listProductsAction(Request $request, Category $category): Response
    {
        $categories = $this->categories->findAllNotEmpty();
        $fields     = $this->products->getSortableFieldNames();

        $filters    = new ProductSearchQuery($request);
        $query      = $this->products->getFindAllPublishedByCategoryQuery($filters, $category);
        $pagination = $this->paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            ProductRepository::MAX_RESULTS
        );

        return $this->render('product/index.html.twig', [
            'active_category' => $category,
            'categories'      => $categories,
            'fields'          => $fields,
            'filters'         => $filters,
            'pagination'      => $pagination,
        ]);
    }
}