<?php
declare(strict_types = 1);

namespace App\Controller;

use App\Entity\User;
use App\Repository\ShoppingCartRepositoryInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Stripe\Charge;
use Stripe\Error\Base;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Stripe\Stripe;
use Symfony\Component\HttpFoundation\Response;
use Tbbc\MoneyBundle\Pair\PairManagerInterface;
use App\ValueObject\CurrencyValue;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class PaymentController extends AbstractController
{
    const BUY_CSRF_TOKEN_ID = 'buy';

    /**
     * @var PairManagerInterface
     */
    private $pairManager;
    /**
     * @var ShoppingCartRepositoryInterface
     */
    private $shoppingCarts;

    /**
     * @param ShoppingCartRepositoryInterface $shoppingCarts
     * @param PairManagerInterface            $pairManager
     */
    public function __construct(
        ShoppingCartRepositoryInterface $shoppingCarts,
        PairManagerInterface $pairManager
    ) {
        $this->pairManager = $pairManager;
        $this->shoppingCarts = $shoppingCarts;
    }

    /**
     * @Route(path="/payment", name="payment")
     * @return Response
     */
    public function paymentAction(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if (null === $user) {
            return $this->redirectToRoute("fos_user_security_login");
        }

        $shoppingCart = $this->shoppingCarts->findOneByUserOrCreate($user);
        $totalPrice = $shoppingCart->getTotalPrice($this->pairManager);
        $description = sprintf('[ORDER #%s]', $shoppingCart->getId());

        return $this->render("shopping/payment.html.twig", [
            'totalPrice'   => $totalPrice,
            'amount'       => $totalPrice->getAmount(),
            'currency'     => CurrencyValue::DEFAULT,
            'description'  => $description,
        ]);
    }

    /**
     * @Route(path="/buy", name="buy")
     * @param Request $request
     * @return Response
     * @throws Base In error case.
     */
    public function buyAction(Request $request): Response
    {
        $token = $request->get('token');
        if (!$this->isCsrfTokenValid(self::BUY_CSRF_TOKEN_ID, $token)) {
            return $this->redirectToRoute('payment');
        }

        /** @var User $user */
        $user = $this->getUser();
        if (null === $user) {
            return $this->redirectToRoute("fos_user_security_login");
        }

        $shoppingCart = $this->shoppingCarts->findOneByUserOrCreate($user);
        $totalPrice = $shoppingCart->getTotalPrice($this->pairManager);

        try {
            Stripe::setApiKey(getenv('STRIPE_API_KEY'));
            $token = $_POST['stripeToken'];
            $charge = Charge::create([
                'amount' => $totalPrice->getAmount(),
                'currency' => CurrencyValue::DEFAULT,
                'description' => $request->get('description'),
                'source' => $token,
            ]);
        } catch (Base $e) {
            throw $e;
        }

        /** @var Charge $charge */
        if ($charge->paid) {
            $shoppingCart->setChargeId($charge->id);
            $this->shoppingCarts->save($shoppingCart);
            $this->addFlash('success', 'Thank you! <3 Now you can buy something else ;)');
            return $this->redirectToRoute('shopping_cart');
        }

        return $this->redirectToRoute('payment');
    }
}