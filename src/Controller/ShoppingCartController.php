<?php
declare(strict_types = 1);

namespace App\Controller;

use App\Entity\CartItem;
use App\Entity\Product;
use App\Entity\ShoppingCart;
use App\Entity\User;
use App\Helpers\Money\MoneyConverter;
use App\Repository\CartItemRepositoryInterface;
use App\Repository\ProductRepository;
use App\Repository\ProductRepositoryInterface;
use App\Repository\ShoppingCartRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use Doctrine\ORM\NonUniqueResultException;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Tbbc\MoneyBundle\Pair\PairManagerInterface;

/**
 * @Route(path="/cart")
 */
class ShoppingCartController extends AbstractController
{
    const ADD_CSRF_TOKEN_ID    = 'add-item';
    const REMOVE_CSRF_TOKEN_ID = 'remove-item';
    const INC_CSRF_TOKEN_ID    = 'inc-quantity';
    const DEC_CSRF_TOKEN_ID    = 'dec-quantity';

    const FILENAME_DELIMITER   = '-';
    const EXPORT_FILE_PREFIX   = 'orders';
    const EXPORT_FILE_FORMAT   = '.csv';

    const IMPORT_FILE_MAX_ROW_LENGTH = 10000;

    const ORDER_ID    = 'orderID';
    const FIRSTNAME   = 'firstname';
    const LASTNAME    = 'lastname';
    const USERNAME    = 'username';
    const EMAIL       = 'e-mail';
    const CHARGE_ID   = 'chargeID';
    const CONFIRMED   = 'confirmed';
    const CREATE_AT   = 'createAt';
    const TOTAL_PRICE = 'totalPrice';
    const TITLE       = 'title';
    const DESCRIPTION = 'description';
    const SKU         = 'SKU';
    const QUANTITY    = 'quantity';
    const PRICE       = 'price';
    const COST        = 'cost';

    const CSV_FILED_NAMES = [
        self::ORDER_ID,
        self::FIRSTNAME,
        self::LASTNAME,
        self::USERNAME,
        self::EMAIL,
        self::CHARGE_ID,
        self::CONFIRMED,
        self::CREATE_AT,
        self::TOTAL_PRICE,
        self::TITLE,
        self::SKU,
        self::QUANTITY,
        self::PRICE,
        self::COST,
    ];

    /**
     * @var ShoppingCartRepositoryInterface
     */
    private $shoppingCarts;
    /**
     * @var CartItemRepositoryInterface
     */
    private $cartItems;
    /**
     * @var PairManagerInterface
     */
    private $pairManager;
    /**
     * @var ProductRepository
     */
    private $products;
    /**
     * @var UserRepositoryInterface
     */
    private $users;
    /**
     * @var UserManagerInterface
     */
    private $userManager;
    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;
    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * @param ShoppingCartRepositoryInterface $shoppingCarts
     * @param CartItemRepositoryInterface     $cartItems
     * @param ProductRepositoryInterface      $products
     * @param UserRepositoryInterface         $users
     * @param PairManagerInterface            $pairManager
     * @param UserManagerInterface            $userManager
     * @param TokenGeneratorInterface         $tokenGenerator
     * @param MailerInterface                 $mailer
     */
    public function __construct(
        ShoppingCartRepositoryInterface $shoppingCarts,
        CartItemRepositoryInterface $cartItems,
        ProductRepositoryInterface $products,
        UserRepositoryInterface $users,
        PairManagerInterface $pairManager,
        UserManagerInterface $userManager,
        TokenGeneratorInterface $tokenGenerator,
        MailerInterface $mailer
    ) {
        $this->shoppingCarts = $shoppingCarts;
        $this->cartItems = $cartItems;
        $this->pairManager = $pairManager;
        $this->products = $products;
        $this->users = $users;
        $this->userManager = $userManager;
        $this->tokenGenerator = $tokenGenerator;
        $this->mailer = $mailer;
    }

    /**
     * @Route(path="/", name="shopping_cart")
     * @return Response
     * @throws \Exception If reqult is not unique.
     */
    public function listItemsAction(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if (null === $user) {
            return new RedirectResponse($this->generateUrl("fos_user_security_login"));
        }

        $shoppingCart = $this->shoppingCarts->findOneByUserOrCreate($user);
        $items = $shoppingCart->getCartItems();
        $totalPrice = $shoppingCart->getTotalPrice($this->pairManager);
        $totalQuantity = $shoppingCart->getTotalQuantity();

        return $this->render('shopping/cart.html.twig', [
            'products' => $items,
            'totalPrice' => $totalPrice,
            'totalQuantity' => $totalQuantity,
        ]);
    }

    /**
     * @Route(path="/add/{id}", name="add_item")
     * @Method({"POST"})
     * @param Request $request
     * @param Product $product
     * @return Response
     * @throws \Exception If reqult is not unique.
     */
    public function addItemAction(Request $request, Product $product): Response
    {
        $token = $request->get('token');
        if (!$this->isCsrfTokenValid(self::ADD_CSRF_TOKEN_ID, $token)) {
            return $this->redirectToRoute('product_list');
        }

        $user = $this->getUser();
        if (null === $user) {
            return new RedirectResponse($this->generateUrl("fos_user_security_login"));
        }

        $shoppingCart = $this->shoppingCarts->findOneByUserOrCreate($user);
        $quantity = (integer) $request->get('quantity');
        $shoppingCart->addItem($this->cartItems, $product, $quantity);

        if ($shoppingCart->hasProduct($product)) {
            $this->addFlash('success', sprintf('Product «%s» added in your shopping card!', $product->getTitle()));
        } else {
            $this->addFlash('danger', 'Something went wrong.Try again!');
        }

        return $this->redirectToRoute('product_list');
    }

    /**
     * @Route(path="/remove/{id}", name="remove_item")
     * @Method({"POST"})
     * @param Request  $request
     * @param CartItem $item
     * @return RedirectResponse
     * @throws \Exception Emmit exception in error case.
     */
    public function removeItemAction(Request $request, CartItem $item): RedirectResponse
    {
        $token = $request->get('token');
        if (!$this->isCsrfTokenValid(self::REMOVE_CSRF_TOKEN_ID, $token)) {
            return $this->redirectToRoute('shopping_cart');
        }

        $user = $this->getUser();
        if (null === $user) {
            return new RedirectResponse($this->generateUrl("fos_user_security_login"));
        }

        $shoppingCart = $item->getShoppingCart();
        $this->denyAccessUnlessGranted('IS_OWNER', $shoppingCart);
        $shoppingCart->removeItem($this->cartItems, $item);

        if (!$shoppingCart->hasProduct($item->getProduct())) {
            $this->addFlash('warning', sprintf('Product «%s» removed from your shopping card!', $item->getProduct()->getTitle()));
        } else {
            $this->addFlash('danger', 'Something went wrong.Try again!');
        }

        return $this->redirectToRoute('shopping_cart');
    }

    /**
     * @Route(path="/inc/{id}", name="inc_quantity")
     * @Method({"POST"})
     * @param Request  $request
     * @param CartItem $item
     * @return Response
     */
    public function incQuantityAction(Request $request, CartItem $item): Response
    {
        $this->denyAccessUnlessGranted('IS_OWNER', $item);

        $token = $request->get('token');
        if ($this->isCsrfTokenValid(self::INC_CSRF_TOKEN_ID, $token)) {
            $cart = $item->getShoppingCart();
            $cart->incrementItemQuantity($this->cartItems, $item);
            $this->shoppingCarts->save($cart);
            $this->addFlash('success', sprintf('+1 %s', $item->getProduct()->getTitle()));
        }

        return $this->redirectToRoute("shopping_cart");
    }

    /**
     * @Route(path="/dec/{id}", name="dec_quantity")
     * @Method({"POST"})
     * @param Request  $request
     * @param CartItem $item
     * @return Response
     */
    public function decQuantityAction(Request $request, CartItem $item): Response
    {
        $this->denyAccessUnlessGranted('IS_OWNER', $item);

        $token = $request->get('token');
        if ($this->isCsrfTokenValid(self::DEC_CSRF_TOKEN_ID, $token)) {
            $cart = $item->getShoppingCart();
            $cart->decrementItemQuantity($this->cartItems, $item);
            $this->shoppingCarts->save($cart);
            $this->addFlash('info', sprintf('-1 %s', $item->getProduct()->getTitle()));
        }

        return $this->redirectToRoute("shopping_cart");
    }

    /**
     * @Route(path="/history", name="shopping_history")
     * @return Response
     */
    public function shoppingHistoryAction(): Response
    {
        $user = $this->getUser();
        if (null === $user) {
            return new RedirectResponse($this->generateUrl("fos_user_security_login"));
        }

        $orders = $this->shoppingCarts->findAllByUser($user);

        return $this->render("shopping/history.html.twig", [
            'orders' => $orders,
        ]);
    }

    /**
     * @Route(path="/csv/export", name="csv_export")
     * @IsGranted("ROLE_ADMIN")
     * @return Response
     */
    public function exportToCsvAction(): Response
    {
        /** @var ShoppingCart[] $carts */
        $carts = $this->shoppingCarts->findAllCharged();

        $filename = self::EXPORT_FILE_PREFIX.
                    self::FILENAME_DELIMITER.
                    date('Y-m-d').
                    self::EXPORT_FILE_FORMAT;

        header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        $fp = fopen('php://output', 'w');

        fputcsv($fp, self::CSV_FILED_NAMES);
        foreach ($carts as $cart) {
            $orderData = [
                $cart->getId(),
                $cart->getUser()->getFirstname(),
                $cart->getUser()->getLastname(),
                $cart->getUser()->getUsername(),
                $cart->getUser()->getEmail(),
                $cart->getChargeId(),
                $cart->getConfirmed(),
                $cart->getCreateAt()->format('d-m-Y'),
                MoneyConverter::toStringReferenceCurrency($cart->getTotalPrice($this->pairManager), $this->pairManager),
            ];

            foreach ($cart->getCartItems() as $item) {
                $product = $item->getProduct();
                $itemData = [
                    $product->getTitle(),
                    $product->getSku(),
                    $item->getQuantity(),
                    MoneyConverter::toStringReferenceCurrency($product->getPrice(), $this->pairManager),
                    MoneyConverter::toStringReferenceCurrency($item->getCost(), $this->pairManager),
                ];

                $row = array_merge($orderData, $itemData);

                fputcsv($fp, $row);
            }
        }

        fclose($fp);

        return new Response();
    }

    /**
     * @Route(path="/csv/import", name="csv_import")
     * @Method({"POST"})
     * @IsGranted("ROLE_ADMIN")
     * @param Request $request
     * @return Response
     * @throws NonUniqueResultException If result is not unique.
     * @throws \Doctrine\ORM\ORMException Emmet exception in error case.
     * @throws \Doctrine\ORM\OptimisticLockException Emmet exception in error case.
     * @throws \Exception Emmet exception in error case.
     */
    public function importFromCsvAction(Request $request): Response
    {
        if ($request->isMethod('POST')) {
            $file = $_FILES['file'];

            if ($file["size"] <= 0) {
                $this->addFlash('warning', 'File is empty or file no chosen');
                return $this->redirectToRoute("easyadmin", ['entity' => 'ShoppingCart', 'action' => 'list']);
            }

            $file = fopen($file["tmp_name"], "r");
            $goodCounter = 0;
            $badCounter  = 0;
            $skipCounter = 0;
            /** @var ShoppingCart $lastShoppingCart */
            $lastShoppingCart = null;

            $fields = fgetcsv($file, self::IMPORT_FILE_MAX_ROW_LENGTH, ",");
            if (self::CSV_FILED_NAMES !== $fields) {
                $this->addFlash('danger', 'Error in field names');
                return $this->redirectToRoute("easyadmin", ['entity' => 'ShoppingCart', 'action' => 'list']);
            }

            while (($row = fgetcsv($file, self::IMPORT_FILE_MAX_ROW_LENGTH, ",")) !== false) {
                $row = array_combine($fields, $row);

                $shoppingCart = $this->shoppingCarts->findByChargeId($row[self::CHARGE_ID]);
                if (null !== $shoppingCart && $lastShoppingCart !== $shoppingCart) {
                    $this->addFlash('warning', sprintf('[Order #%s] Skipped -- This order has not unique chargeID, which already is used', $shoppingCart->getId()));
                    $skipCounter++;
                    continue;
                }

                $product = $this->products->findOneBySku($row[self::SKU]);
                if (null === $product) {
                    $product = new Product();
                    $product->setTitle($row[self::TITLE]);
                    $product->setSku($row[self::SKU]);
                    $product->setPrice(MoneyConverter::toMoney($row[self::PRICE]));
                    $product->setDescription('-');
                    $this->products->save($product);
                    $this->addFlash('default', sprintf('[Order #%s] New product -- %s', $row[self::ORDER_ID], $row[self::TITLE]));
                }

                /** @var User $user */
                $user = $this->users->findOneByEmail($row[self::EMAIL]);
                if (null === $user) {
                    $user = $this->users->findOneByUsername($row[self::USERNAME]);
                    if (null !== $user) {
                        $this->addFlash('warning', sprintf('[Order #%s] Skipped -- Username %s is already used', $row[self::ORDER_ID], $row[self::USERNAME]));
                        $skipCounter++;
                        continue;
                    }

                    $user = $this->userManager->createUser();
                    $user->setFirstname($row[self::FIRSTNAME]);
                    $user->setLastname($row[self::LASTNAME]);
                    $user->setUsername($row[self::USERNAME]);
                    $user->setEmail($row[self::EMAIL]);

                    $password = $this->tokenGenerator->generateToken();
                    $user->setPlainPassword($password);
                    $token = $this->tokenGenerator->generateToken();

                    $this->userManager->updateUser($user);
                    $user->setConfirmationToken($token);
                    $this->mailer->sendConfirmationEmailMessage($user);
                    $this->addFlash('default', sprintf('[Order #%s] New user -- %s (%s)', $row[self::ORDER_ID], $row[self::USERNAME], $row[self::EMAIL]));
                }

                $shoppingCart = null;
                if ($lastShoppingCart !== null &&
                    $lastShoppingCart->getChargeId() === $row[self::CHARGE_ID]) {
                    $shoppingCart = $lastShoppingCart;
                } else {
                    $shoppingCart = $this->shoppingCarts->findOneByUserOrCreate($user);
                }

                $quantity = (integer) $row[self::QUANTITY];
                $shoppingCart->addItem($this->cartItems, $product, $quantity);
                if ($this->cartItems->itemInCart($product, $shoppingCart)) {
                    $this->addFlash('success', sprintf('[Order #%s] Successfully imported', $row[self::ORDER_ID]));
                    $goodCounter++;
                } else {
                    $this->addFlash('danger', sprintf('[Order #%s] Import error', $row[self::ORDER_ID]));
                    $badCounter++;
                }

                $shoppingCart->setChargeId($row[self::CHARGE_ID]);
                $shoppingCart->setConfirmed((boolean) $row[self::CONFIRMED]);
                $this->shoppingCarts->save($shoppingCart);
                $lastShoppingCart = $shoppingCart;
            }
            $this->addFlash('info', sprintf('CSV Data Imported into the Database. (%d -- skipped / %d -- success / %d -- with errors)', $skipCounter, $goodCounter, $badCounter));
        }

        return $this->redirectToRoute("easyadmin", ['entity' => 'ShoppingCart', 'action' => 'list']);
    }
}