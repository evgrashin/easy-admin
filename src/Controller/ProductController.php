<?php

namespace App\Controller;

use App\Entity\Product;
use App\Query\ProductSearchQuery;
use App\Repository\CategoryRepositoryInterface;
use App\Repository\ProductRepository;
use App\Repository\ProductRepositoryInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/product")
 */
class ProductController extends AbstractController
{
    /**
     * @var ProductRepositoryInterface
     */
    private $products;
    /**
     * @var CategoryRepositoryInterface
     */
    private $categories;
    /**
     * @var PaginatorInterface
     */
    private $paginator;

    /**
     * @param ProductRepositoryInterface  $products
     * @param CategoryRepositoryInterface $categories
     * @param PaginatorInterface          $paginator
     */
    public function __construct(
        ProductRepositoryInterface $products,
        CategoryRepositoryInterface $categories,
        PaginatorInterface $paginator
    ) {
        $this->products = $products;
        $this->categories = $categories;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/", name="product_list")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        $categories = $this->categories->findAllNotEmpty();
        $fields     = $this->products->getSortableFieldNames();

        $filters    = new ProductSearchQuery($request);
        $query      = $this->products->getFindAllPublishedQuery($filters);
        $pagination = $this->paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            ProductRepository::MAX_RESULTS
        );

        return $this->render('product/index.html.twig', [
            'categories' => $categories,
            'fields'     => $fields,
            'filters'    => $filters,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/{id}", name="product_show")
     * @param Product $product
     * @return Response
     */
    public function showAction(Product $product): Response
    {
        return $this->render('product/show.html.twig', ['product' => $product]);
    }
}
