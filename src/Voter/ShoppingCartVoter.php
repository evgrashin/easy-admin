<?php
declare(strict_types = 1);

namespace App\Voter;

use App\Entity\ShoppingCart;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class ShoppingCartVoter extends Voter
{
    const IS_OWNER = 'IS_OWNER';

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        if ($attribute !== self::IS_OWNER) {
            return false;
        }

        if (!$subject instanceof ShoppingCart) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        $shoppingCart = $subject;

        return $this->checkIfUserIsOwner($shoppingCart, $user);
    }

    /**
     * @param ShoppingCart $shoppingCart
     * @param User         $user
     * @return boolean
     */
    private function checkIfUserIsOwner(ShoppingCart $shoppingCart, User $user)
    {
        return $user === $shoppingCart->getUser();
    }
}