<?php
declare(strict_types = 1);

namespace App\Voter;

use App\Entity\CartItem;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class CartItemVoter extends Voter
{
    const IS_OWNER = 'IS_OWNER';

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        if ($attribute !== self::IS_OWNER) {
            return false;
        }

        if (!$subject instanceof CartItem) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        $shoppingCart = $subject;

        return $this->checkIfUserIsOwner($shoppingCart, $user);
    }

    /**
     * @param CartItem $cartItem
     * @param User     $user
     * @return boolean
     */
    private function checkIfUserIsOwner(CartItem $cartItem, User $user)
    {
        return $user === $cartItem->getUser();
    }
}