<?php
declare(strict_types = 1);

namespace App\Helpers\Money;

use App\ValueObject\CurrencyValue;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Money;
use Tbbc\MoneyBundle\Pair\PairManagerInterface;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class MoneyConverter
{
    const CURRENCY_CODE_LENGTH = 3;

    /**
     * @param Money $money
     * @return string
     */
    public static function toString(Money $money): string
    {
        return sprintf('%s %.2f', $money->getCurrency(), $money->getAmount()/100);
    }

    /**
     * @param Money                $money
     * @param PairManagerInterface $pairManager
     * @return Money
     */
    public static function toReferenceCurrency(Money $money, PairManagerInterface $pairManager): Money
    {
        return $pairManager->convert($money, CurrencyValue::DEFAULT);
    }

    /**
     * @param Money                $money
     * @param PairManagerInterface $pairManager
     * @return string
     */
    public static function toStringReferenceCurrency(Money $money, PairManagerInterface $pairManager): string
    {
        return self::toString(self::toReferenceCurrency($money, $pairManager));
    }

    /**
     * @param string $string
     * @return Money
     */
    public static function toMoney(string $string): Money
    {
        $currencyCode = substr($string, 0, self::CURRENCY_CODE_LENGTH);
        $amount = (int) preg_replace('~\D~', '', $string);
        $currency = new Currency($currencyCode);
        $money = new Money($amount, $currency);

        return $money;
    }
}