<?php
declare(strict_types = 1);

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class SkuValidator extends ConstraintValidator
{
    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof Sku) {
            throw new UnexpectedTypeException($constraint, Sku::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_scalar($value) && !(\is_object($value) && method_exists($value, '__toString'))) {
            throw new UnexpectedValueException($value, 'string');
        }

        $string = (string) $value;

        if (strlen($string) < Sku::MIN_LENGTH ||
            strlen($string) > Sku::MAX_LENGTH ||
            !$this->hasDigitsLettersDashesOnly($string)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $string)
                ->setParameter('{{ min_limit }}', SKU::MIN_LENGTH)
                ->setParameter('{{ max_limit }}', SKU::MAX_LENGTH)
                ->addViolation();
        }
    }

    /**
     * @param string $string
     * @return boolean
     */
    private function hasDigitsLettersDashesOnly(string $string)
    {
        preg_match('/^[A-Za-z\d][A-Za-z\d-]*[A-Za-z\d]$/', $string, $matches);
        if (count($matches) == 1 && $matches[0] == $string) {
            return true;
        }
        return false;
    }
}