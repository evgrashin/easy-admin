<?php
declare(strict_types = 1);

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class Sku extends Constraint
{
    const MIN_LENGTH = 10;
    const MAX_LENGTH = 20;

    public $message = "SKU should have digits, letters and dashes only. Dashes on first or last position are not valid. Length from {{ min_limit }} to {{ max_limit }}.";
}