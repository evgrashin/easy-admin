<?php
declare(strict_types = 1);

namespace App\Faker\Provider;

use App\ValueObject\CurrencyValue;
use Faker\Provider\Base as BaseProvider;
use Money\Currency;
use Money\Money;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
final class PriceProvider extends BaseProvider
{
    const MIN_PRICE = 1;
    const MAX_PRICE = 9999;

    /**
     * @return Money
     */
    public function price(): Money
    {
        $currency_key  = array_rand(CurrencyValue::LIST);
        $priceCurrency = new Currency(CurrencyValue::LIST[$currency_key]);
        $priceAmount   = mt_rand(self::MIN_PRICE, self::MAX_PRICE);
        $price         = new Money($priceAmount, $priceCurrency);

        return $price;
    }
}