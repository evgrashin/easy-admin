<?php
declare(strict_types = 1);

namespace App\Query;

use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Oleg Evgrashin <oleg.evgrashin@sibers.com>
 */
class ProductSearchQuery
{
    /**
     * @var string
     */
    private $sortBy;
    /**
     * @var string
     */
    private $sortDir;

    const SORT_BY_KEY        = 'sort_by';
    const SORT_DIRECTION_KEY = 'sort_dir';

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->sortBy = $request->get(self::SORT_BY_KEY) ?? ProductRepository::COLUMN_ID;
        $this->sortDir = $request->get(self::SORT_DIRECTION_KEY) ?? ProductRepository::SORT_DIRECTION_DESC;
    }

    /**
     * @return string
     */
    public function getSortBy(): string
    {
        return $this->sortBy;
    }

    /**
     * @param string $sortBy
     */
    public function setSortBy(string $sortBy): void
    {
        $this->sortBy = $sortBy;
    }

    /**
     * @return string
     */
    public function getSortDir(): string
    {
        return $this->sortDir;
    }

    /**
     * @param string $sortDir
     */
    public function setSortDir(string $sortDir): void
    {
        $this->sortDir = $sortDir;
    }
}